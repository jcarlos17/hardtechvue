const path = require("path");

// const anyBody = require('body/any');

module.exports = {
    publicPath:
        process.env.NODE_ENV === "production" ? "/metronic/vue/demo1/" : "/",

    configureWebpack: {
        devServer: {
            proxy: {
                "/": {
                    target: 'https://staginghardtech.siansystem.com/admin/apiPos',
                    secure: false,
                    changeOrigin: true,
                    /*
                    onProxyReq: (proxyReq) => {
                        
                        const authHeader = proxyReq.getHeader('Authorization');
                        if (authHeader) {
                            console.log(authHeader);
                            proxyReq.setHeader('Authorization', authHeader);
                        }
                        
                    }*/
                }
            }
        },

        resolve: {
            alias: {
                // If using the runtime only build
                vue$: "vue/dist/vue.runtime.esm.js" // 'vue/dist/vue.runtime.common.js' for webpack 1
                // Or if using full build of Vue (runtime + compiler)
                // vue$: 'vue/dist/vue.esm.js'      // 'vue/dist/vue.common.js' for webpack 1
            }
        }
    },

    chainWebpack: config => {

    },
    css: {
        loaderOptions: {
            postcss: {
                config: {
                    path: __dirname
                }
            },
            scss: {
                prependData: `@import "@/assets/sass/vendors/vue/vuetify/variables.scss";`
            }
        }
    },
    transpileDependencies: ["vuetify"]
};
