import ApiService from "@/core/services/api.service";
import {SET_AUTH} from "./auth.module";

// action types
export const SEARCH_BY_CODE = "searchByCode";
export const SEARCH_BY_NAME = "searchByName";

// mutation types
export const GO_TO_STEP = "setStep";
export const SET_INPUT = "setSelectedInput";
export const SET_INPUT_VALUE = "setValueToInput";

export const SET_QUERY = "setQuery";
export const SET_DOCUMENT = "setDocument";
export const SET_CLIENT = "setClient";
export const SET_RESULTS = "setResults";

export const ADD_CART_ITEM = "addCartItem";
export const REMOVE_CART_ITEM = "removeCartItem";
export const EDIT_CART_ITEM_QTY = "editCartItemQty";
export const CLEAR_CART = "clearCart";

export const NUMERIC_KEYBOARD_CLEAN = "numericKeyboardClean";
export const NUMERIC_KEYBOARD_APPEND = "numericKeyboardAppend";
export const NUMERIC_KEYBOARD_ACCEPT = "numericKeyboardAccept";


const state = {
    step: 1,
    
    query: '',
    results: [],
    
    document: '',
    client: '',
    inputModel: null,

    list: [],

    editQtyIndex: null
};

const getters = {
    subTotal(state, getters) {
        const subTotal = getters.total - getters.igv;
        return Math.round(subTotal * 100) / 100
    },
    igv(state, getters) {
        const igv = getters.total * 0.18;
        return Math.round(igv * 100) / 100;
    },
    total(state) {
        let sum = 0;

        state.list.forEach((e, i) => {
            sum += e.iaprice * e.quantity;
        });

        return Math.round(sum * 100) / 100;
    }
};

const actions = {
    [SEARCH_BY_CODE]({ commit, state }, code) {
        const url = `merchandise?filters[equal][product_id]=${code}&presentation_mode=ALL`;

        ApiService.setHeader();
        
        return ApiService.get(url)
            .then(({data}) => {
                const responseCode = data.code;
                if (responseCode !== 200)
                    return null;
                
                const responseBody = data.data;
                const records = responseBody.records;
                
                if (records === 0)
                    return null;
                
                return responseBody.rows[0];
            });
    },
    [SEARCH_BY_NAME]({ commit, state }) {
        const url = `merchandise?filters[like][product_name]=${state.query}&limit=10&page=1`;

        ApiService.setHeader();

        return ApiService.get(url)
            .then(({data}) => {
                const responseCode = data.code;
                if (responseCode !== 200)
                    return null;

                const responseBody = data.data;
                const records = responseBody.records;

                if (records === 0)
                    return 0;

                commit(SET_RESULTS, responseBody.rows);
                commit(GO_TO_STEP, 2);
                
                return records;
            });
    },
};

const mutations = {
    [GO_TO_STEP](state, step) {
        state.step = step;
    },
    [SET_INPUT](state, inputModel) {
        state.inputModel = inputModel;
        // console.log('state.inputModel value', state.inputModel);
    },
    [SET_INPUT_VALUE](state, newValue) {
        state[state.inputModel] = newValue;
    },

    [SET_QUERY](state, query) {
        state.query = query;
    },
    [SET_DOCUMENT](state, document) {
        state.document = document;  
    },
    [SET_CLIENT](state, client) {
        state.client = client;
    },
    [SET_RESULTS](state, results) {
        state.results = results;
    },

    [ADD_CART_ITEM](state, item) {
        state.list.push(item);
    },
    [REMOVE_CART_ITEM](state, index) {
        state.list.splice(index, 1);
    },
    [EDIT_CART_ITEM_QTY](state, index) {
        state.editQtyIndex = index;
    },
    [CLEAR_CART](state) {
        state.list = [];
    },

    [NUMERIC_KEYBOARD_CLEAN](state) {        
        if (state.editQtyIndex !== null) {
            state.list[state.editQtyIndex].quantity = 0;
        }            
    },
    [NUMERIC_KEYBOARD_APPEND](state, n) {
        if (state.editQtyIndex !== null) {
            const strQty = state.list[state.editQtyIndex].quantity.toString();
            const newQty = strQty + n.toString();
            
            state.list[state.editQtyIndex].quantity = parseInt(newQty);
        }
    },
    [NUMERIC_KEYBOARD_ACCEPT](state) {
        state.editQtyIndex = null;
    },
};

export default {
    state,
    actions,
    mutations,
    getters
};
