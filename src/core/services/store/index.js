import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth.module";
import pos from "./pos.module";
import htmlClass from "./htmlclass.module";
import config from "./config.module";
import breadcrumbs from "./breadcrumbs.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        pos,
        htmlClass,
        config,
        breadcrumbs
    }
});
